package net.asafaie.checkoutdemo;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.models.SpecialPricing;
import net.asafaie.checkoutdemo.services.Checkout;
import net.asafaie.checkoutdemo.services.CheckoutService;
import net.asafaie.checkoutdemo.services.ProductInMemoryService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String...args) throws Exception {

        ProductInMemoryService productInMemoryService = new ProductInMemoryService();
        Product pA = productInMemoryService.createProduct("A", new BigDecimal("50"));
        Product pB = productInMemoryService.createProduct("B", new BigDecimal("30"));
        productInMemoryService.createProduct("C", new BigDecimal("20"));
        productInMemoryService.createProduct("D", new BigDecimal("15"));

        SpecialPricing pricingRuleA = new SpecialPricing(pA,3,new BigDecimal("130"));
        SpecialPricing pricingRuleB = new SpecialPricing(pB,2,new BigDecimal("45"));
        List<SpecialPricing> pricingRules  = Arrays.asList(pricingRuleA,pricingRuleB);
        Checkout checkout = new CheckoutService(pricingRules);

        Scanner scanner = new Scanner(System.in);
        boolean run = true;
        System.out.println("This application has 4 Product with SKU , A , B , C , D ");
        while (run){
            System.out.println("Scan Product:");
            String input = scanner.nextLine();
            if("EXIT".equalsIgnoreCase(input)){
                run = false;
            }
            if("final".equalsIgnoreCase(input)){
                BigDecimal total =  checkout.total();
                System.out.println("Total $"+total.toPlainString());
                run = false;
            }
            if(run) {
                Product pr = productInMemoryService.getProduct(input);
                if (pr == null) {
                    System.out.println("Product Don't Exist");
                } else {
                    checkout.scan(pr);
                }
            }

        }
        scanner.close();
        System.exit(0);


    }
}
