package net.asafaie.checkoutdemo.models;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpecialPricing {

    private Product product;

    private int qty;

    private BigDecimal specialPrice;

    public SpecialPricing(Product product, int qty, BigDecimal specialPrice) throws IllegalArgumentException{

        if(product == null){
            throw new IllegalArgumentException("Product can't be null");
        }
        if(qty < 1){
            throw new IllegalArgumentException("Qty can't be less than 1");
        }

        this.product = product;
        this.qty = qty;
        this.specialPrice = specialPrice;
    }

    @Override
    public String toString() {
        return "On Specials -- EACH " + qty + " for $"+ specialPrice.toPlainString();
    }
}
