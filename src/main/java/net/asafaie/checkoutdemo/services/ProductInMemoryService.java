package net.asafaie.checkoutdemo.services;


import net.asafaie.checkoutdemo.models.Product;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Store Data in Memory
 * @author A.H.SAfaie
 */
public class ProductInMemoryService {

    // Each product mapped to SKU
    private final Map<String, Product> products ;

    public ProductInMemoryService() {
        this.products = new HashMap<>();
    }

    public Product getProduct(String SKU){
        return  products.getOrDefault(SKU,null);
    }


    public Product createProduct(String sku, BigDecimal individualPrice) throws Exception {
        Product product = getProduct(sku);
        if(product != null){
            throw  new Exception("Product with " + sku + " already exist.");
        }
        product = new Product(sku, individualPrice);
        products.put(sku,product);
        return product;
    }

    public Product updateProduct(String sku, BigDecimal individualPrice) throws Exception {
        Product product = getProduct(sku);
        if(product == null){
            throw  new Exception("Product with " + sku + " not exist.");
        }
        product.setIndividualPrice(individualPrice);
        products.put(sku,product);
        return product;
    }

}
