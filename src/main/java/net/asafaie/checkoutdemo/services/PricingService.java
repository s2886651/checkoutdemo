package net.asafaie.checkoutdemo.services;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.models.SpecialPricing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author A.H.Safaie
 *
 */
public class PricingService {



    private final Logger LOGGER = LoggerFactory.getLogger(PricingService.class);

    private final Map<Product, List<SpecialPricing>> pricingRules;

    public PricingService(List<SpecialPricing> pricingRuleList) {
        this.pricingRules = pricingRuleList != null ?
                pricingRuleList.stream().collect(Collectors.groupingBy(SpecialPricing::getProduct,Collectors.toList()))
                : new HashMap<>();
    }


    public BigDecimal getTotalForProductByQty(Product product , int totalQty){
        if(totalQty == 0 || product == null ){ return BigDecimal.ZERO; }
        List<SpecialPricing> availablePricing = this.pricingRules.get(product);

        LOGGER.debug("Product SKU {} price for each ${}",product.getSKU(),product.getIndividualPrice());
        // Has Special pricing Use Pricing Service
        if(availablePricing != null && !availablePricing.isEmpty()){
            return this.calculateBySpecialPricing(availablePricing.get(0),totalQty);
        }

        BigDecimal total =  product.getIndividualPrice().multiply(BigDecimal.valueOf(totalQty)).setScale(2,RoundingMode.HALF_EVEN);
        LOGGER.debug("QTY {} for price of ${}",totalQty,total);
        return total;
    }


    private BigDecimal calculateBySpecialPricing(SpecialPricing specialPricing , int totalQty ){
        // Number of Items need to use individual pricing
        int individualPriced = totalQty % specialPricing.getQty();
        // Number of group discounts
        int pricingUsed =totalQty / specialPricing.getQty();

        BigDecimal totalWithDiscount = specialPricing.getSpecialPrice().multiply(BigDecimal.valueOf(pricingUsed)).setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal totalWithoutDiscount = specialPricing.getProduct().getIndividualPrice().multiply(BigDecimal.valueOf(individualPriced)).setScale(2,RoundingMode.HALF_EVEN);

        LOGGER.debug(specialPricing.toString());

        if(pricingUsed > 0) {
            LOGGER.debug("QTY {} with total price of {}", pricingUsed * specialPricing.getQty(), totalWithDiscount);
        }

        if(individualPriced > 0) {
            LOGGER.debug("QTY {} for price of ${}", individualPriced, totalWithoutDiscount);
        }
        return totalWithDiscount.add(totalWithoutDiscount).setScale(2,RoundingMode.HALF_EVEN);
    }



}
