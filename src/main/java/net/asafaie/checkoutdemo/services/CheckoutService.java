package net.asafaie.checkoutdemo.services;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.models.SpecialPricing;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckoutService implements Checkout{

    private final Map<Product,Integer> bag;


    private final PricingService pricingService;


    public CheckoutService(List<SpecialPricing> pricingRuleList) {
        this.bag = new HashMap<>();
        this.pricingService = new PricingService(pricingRuleList);
   }

    @Override
    public void scan(Product item) {
        this.bag.put(item,this.bag.computeIfAbsent(item,t-> 0)+1);
    }

    @Override
    public void remove(Product item) {
        this.bag.put(item,this.bag.computeIfAbsent(item,t-> 1) -1 );
    }

    @Override
    public int getItemQty(Product product){
        return this.bag.getOrDefault(product, 0);
    }

    @Override
    public BigDecimal total() {
        BigDecimal  total = BigDecimal.ZERO;
        for (Map.Entry<Product, Integer> productIntegerEntry : this.bag.entrySet()) {
            Product product = productIntegerEntry.getKey();
            total =  total.add(pricingService.getTotalForProductByQty(product,productIntegerEntry.getValue()));
        }
        return total.setScale(2,RoundingMode.HALF_EVEN);
    }
}
