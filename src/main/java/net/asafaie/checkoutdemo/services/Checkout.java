package net.asafaie.checkoutdemo.services;

import net.asafaie.checkoutdemo.models.Product;

import java.math.BigDecimal;

public interface Checkout {

    void scan(Product item);

    void remove(Product item);

    int getItemQty(Product product);

    BigDecimal total();

}
