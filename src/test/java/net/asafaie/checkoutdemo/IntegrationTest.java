package net.asafaie.checkoutdemo;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.models.SpecialPricing;
import net.asafaie.checkoutdemo.services.Checkout;
import net.asafaie.checkoutdemo.services.CheckoutService;
import net.asafaie.checkoutdemo.services.ProductInMemoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author A.H.Safaie
 * Adding all together
 */
public class IntegrationTest {

    ProductInMemoryService productService;

    @BeforeEach
    void setUp() {
        this.productService = new ProductInMemoryService();
    }

    // All together


    private List<Product> createAllProducts() throws Exception {
        Product pA = this.productService.createProduct("A", new BigDecimal("50"));
        Product pB = this.productService.createProduct("B", new BigDecimal("30"));
        Product pC = this.productService.createProduct("C", new BigDecimal("20"));
        Product pD = this.productService.createProduct("D", new BigDecimal("15"));
        return Arrays.asList(pA,pB,pC,pD);
    }

    @Test
    public void testAllTogether_addAllProductsWithSpecialPricingGetTotalCheckout_totalShouldMatchManualCalculation370() throws Exception {

        Product pA = this.productService.createProduct("A", new BigDecimal("50"));
        Product pB = this.productService.createProduct("B", new BigDecimal("30"));
        Product pC = this.productService.createProduct("C", new BigDecimal("20"));
        Product pD = this.productService.createProduct("D", new BigDecimal("15"));

        SpecialPricing pricingRuleA = new SpecialPricing(pA,3,new BigDecimal("130"));
        SpecialPricing pricingRuleB = new SpecialPricing(pB,2,new BigDecimal("45"));

        List<SpecialPricing> pricingRules  = Arrays.asList(pricingRuleA,pricingRuleB);
        Checkout checkout = new CheckoutService(pricingRules);
        checkout.scan(pA);
        checkout.scan(pC);
        checkout.scan(pA);
        checkout.scan(pB);
        checkout.scan(pA);
        checkout.scan(pB);
        checkout.scan(pB);
        checkout.scan(pC);
        checkout.scan(pD);
        checkout.scan(pB);
        checkout.scan(pD);
        checkout.scan(pA);
        checkout.scan(pB);
        BigDecimal totalValue = checkout.total();
        assertThat(totalValue).isEqualTo(new BigDecimal("370.00"));
    }

    @Test
    public void randomCalculation_addRandomProductsAndCheckCalculation_totalItems()  throws Exception{

        List<Product> listOfProducts = createAllProducts();
        SpecialPricing pricingRuleA = new SpecialPricing(listOfProducts.get(0),3,new BigDecimal("130"));
        SpecialPricing pricingRuleB = new SpecialPricing(listOfProducts.get(1),2,new BigDecimal("45"));
        Random random= new Random();
        List<SpecialPricing> pricingRules  = Arrays.asList(pricingRuleA,pricingRuleB);
        Checkout checkout = new CheckoutService(pricingRules);
        IntStream.range(0,25).forEach(t->{
            int productsIndex =  random.nextInt(listOfProducts.size());
            checkout.scan(listOfProducts.get(productsIndex));
        });

        BigDecimal total = checkout.total();
        System.out.println("Total is $"+total.toPlainString());
        assertThat(total).isNotEqualTo(BigDecimal.ZERO);
    }


}
