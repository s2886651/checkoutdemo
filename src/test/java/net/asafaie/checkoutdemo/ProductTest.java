package net.asafaie.checkoutdemo;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.services.ProductInMemoryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testing Product class
 *
 */
public class ProductTest {


    private ProductInMemoryService productInMemoryService;

    @BeforeEach
    void setUp() {
        this.productInMemoryService = new ProductInMemoryService();
    }

    @AfterEach
    void tearDown() {
        this.productInMemoryService = null;
    }

    @Test
    public void testCreateProduct_createOneProduct_productIsNotNull(){
        Product product = new Product();
        assertThat(product).isNotNull();
    }

    @Test
    public void testCreateProduct_createProductWithSKU_productNameIsEquals() throws Exception {
        String SKU = "A";
        Product product = this.productInMemoryService.createProduct(SKU,null);
        assertThat(product.getSKU()).isEqualTo(SKU);
    }

    @Test
    public void testCreateProduct_createProductWithNameAndPrice_productNameAndPriceIsEquals() throws Exception {
        Product product =  this.productInMemoryService.createProduct("A",new BigDecimal("2.30"));
        assertThat(product.getIndividualPrice()).isEqualTo(BigDecimal.valueOf(2.30).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void testCreatingDuplicateProduct_createDuplicateProductWithSameSKU_ExceptionMessageContainTheStringValue(){
        Exception exception = assertThrows(Exception.class, () -> {
             this.productInMemoryService.createProduct("A", new BigDecimal("2.30"));
             this.productInMemoryService.createProduct("A", new BigDecimal("2.30"));
        });
        assertThat(exception.getMessage()).isEqualToIgnoringCase("Product with A already exist.");
    }


    @Test
    public void updateProduct_createProductAndUpdateThePrice_priceIsEqualsToNewPrice() throws Exception{
        String SKU = "A";
        this.productInMemoryService.createProduct(SKU,new BigDecimal("2.30"));
        this.productInMemoryService.updateProduct(SKU, new BigDecimal("2.50"));
        assertThat(this.productInMemoryService.getProduct(SKU).getIndividualPrice()).isEqualTo(new BigDecimal("2.50"));
    }

    @Test
    public void testUpdatingProduct_updateProductThatDontExist_ExceptionMessageContainTheStringValue(){
        Exception exception = assertThrows(Exception.class, () -> {
            this.productInMemoryService.updateProduct("A", new BigDecimal("2.30"));
        });
        assertThat(exception.getMessage()).isEqualToIgnoringCase("Product with A not exist.");
    }

}
