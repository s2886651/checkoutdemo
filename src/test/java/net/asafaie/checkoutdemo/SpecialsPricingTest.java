package net.asafaie.checkoutdemo;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.models.SpecialPricing;
import net.asafaie.checkoutdemo.services.ProductInMemoryService;
import net.asafaie.checkoutdemo.services.PricingService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author A.H.Safaie
 * Test Special Pricing Class
 */
public class SpecialsPricingTest {


    private ProductInMemoryService productInMemoryService;




    @BeforeEach
    void setUp() {
        this.productInMemoryService = new ProductInMemoryService();

    }

    @AfterEach
    void tearDown() {
        this.productInMemoryService = null;

    }

    @Test
    public void createSpecialPricing_createPricingRuleForProduct_schedulePricingProductIsNotNull() throws Exception {
        Product product =this.productInMemoryService.createProduct("A",new BigDecimal("2.30") );
        SpecialPricing pricingRule = new SpecialPricing(product,1,BigDecimal.ZERO);
        assertThat(pricingRule.getProduct()).isNotNull();
    }

    @Test
    public void createSpecialPricing_CreateSpecialPricingWithQTY_hasQtyOf1(){
        Product productA = new Product("A",new BigDecimal("5.00") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,3,BigDecimal.ZERO);
        assertThat(pricingRuleA.getQty()).isEqualTo(3);
    }



    @Test
    public void createSpecialPricing_CreateSpecialPricingWithSpecialPrice_hasSpecialPriceOf5(){
        Product productA = new Product("A",new BigDecimal("5.00") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,3,new BigDecimal("130"));
        assertThat(pricingRuleA.getSpecialPrice()).isEqualTo(new BigDecimal("130"));
    }

    @Test
    public void createSpecialPricing_CreateSpecialPricingWithoutProduct_throwsIllegalException(){
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,()->
                new SpecialPricing(null,3,new BigDecimal("130")));
        assertThat(exception.getMessage()).isEqualToIgnoringCase("Product can't be null");
    }
    @Test
    public void createSpecialPricing_CreateSpecialPricingWith0QTY_throwsIllegalException(){
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,()->
                new SpecialPricing(new Product("A",new BigDecimal("5.00") ),0,new BigDecimal("130")));
        assertThat(exception.getMessage()).isEqualToIgnoringCase("Qty can't be less than 1");
    }

    @Test
    public void testCreateSpecialPiercings_createMultiplePricingRuleForMultipleProducts_productAisAvailableInPricingRule() throws Exception {
        Product productA = this.productInMemoryService.createProduct("A",new BigDecimal("2.30") );
        Product productB = this.productInMemoryService.createProduct("B",new BigDecimal("2.50") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,1,BigDecimal.ZERO);
        SpecialPricing pricingRuleB = new SpecialPricing(productB,1,BigDecimal.ZERO);
        List<SpecialPricing> priceRules = Arrays.asList(pricingRuleA,pricingRuleB);
        Map<Product, List<SpecialPricing>> map = priceRules.stream().collect(Collectors.groupingBy(SpecialPricing::getProduct, Collectors.toList()));
        assertThat(map.get(productA)).isNotEmpty();
    }

    @Test
    public void testSpecialPricingCalculation_totalByePricingIsNotSameAsTotalByproductAndQty_valueTotalIsNotEqual(){
        Product productA = new Product("A",new BigDecimal("5.00") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,3,new BigDecimal("12.00"));
        assertThat(pricingRuleA.getSpecialPrice()).isNotEqualTo(productA.getIndividualPrice().multiply(new BigDecimal("3.00")).setScale(2, RoundingMode.HALF_EVEN));
    }


    @Test
    public void testTotalsSpecialPricing_totalNumberSpecialPricingFor3UsedFor7SameProduct_NumberOfProductUsingPricingIs2(){
        Product productA = new Product("A",new BigDecimal("5.00") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,3,new BigDecimal("12.00"));
        int totalQty = 7;
        int pricingUsed = totalQty / pricingRuleA.getQty();
        assertThat(pricingUsed).isEqualTo(2);
    }

    @Test
    public void testCalculateTotal_usePricingToCalculatedlyPriceOf7ProductsForSpecialPricingBy3QTY_totalIsEquals29(){
        // total is( 5.00 * ((7%3) = 1))  + (12.00 * (7/3 = 2)  ) = 29.00
        Product productA = new Product("A",new BigDecimal("5.00") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,3,new BigDecimal("12.00"));
        int totalQty = 7;
        int individualPriced = totalQty % pricingRuleA.getQty();
        int pricingUsed =totalQty / pricingRuleA.getQty();

        BigDecimal total = pricingRuleA.getSpecialPrice().multiply(BigDecimal.valueOf(pricingUsed))
                .setScale(2,RoundingMode.HALF_EVEN).add(productA.getIndividualPrice()
                        .multiply(BigDecimal.valueOf(individualPriced))
                        .setScale(2,RoundingMode.HALF_EVEN))
                .setScale(2,RoundingMode.HALF_EVEN);
        assertThat(total).isEqualTo(new BigDecimal("29.00"));
    }


    @Test
    public void testGetTotalForProductByQty_totalCalculationInPricingService_totalIsEquals29(){
        Product productA = new Product("A",new BigDecimal("5.00") );
        SpecialPricing pricingRuleA = new SpecialPricing(productA,3,new BigDecimal("12.00"));
        int totalQty = 7;
        PricingService pricingService  = new PricingService(Collections.singletonList(pricingRuleA));
        BigDecimal total = pricingService.getTotalForProductByQty(productA, totalQty);
        assertThat(total).isEqualTo(new BigDecimal("29.00"));
    }






}
