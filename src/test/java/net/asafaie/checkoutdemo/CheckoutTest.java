package net.asafaie.checkoutdemo;

import net.asafaie.checkoutdemo.models.Product;
import net.asafaie.checkoutdemo.models.SpecialPricing;
import net.asafaie.checkoutdemo.services.Checkout;
import net.asafaie.checkoutdemo.services.CheckoutService;
import net.asafaie.checkoutdemo.services.ProductInMemoryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author A.H.Safaie
 */
public class CheckoutTest {

    ProductInMemoryService productService;

    @BeforeEach
    void setUp() {
        this.productService = new ProductInMemoryService();
    }

    @AfterEach
    void tearDown() {
        this.productService = null;
    }

    @Test
    public void createOrder_createOrderAndItems_orderHas2Items() throws Exception {
        Checkout checkout = new CheckoutService(null);
        Product productA =productService.createProduct("A",null);
        checkout.scan(productA);
        checkout.scan(productA);
        assertThat(checkout.getItemQty(productA)).isEqualTo(2);
    }

    @Test
    public void testScan_calculateTotalCheckoutForProductsWith10Qty_totalShouldBe50() throws Exception {
        Product  productA = this.productService.createProduct("A",new BigDecimal("5"));
        Checkout checkout =  new CheckoutService(null);
        IntStream.range(0,10).forEach(t->checkout.scan(productA));
        assertThat(checkout.getItemQty(productA)).isEqualTo(10);
    }

    @Test
    public void testRemoving_calculateTotalByAdding20AndRemoving10_totalShouldBe50() throws Exception {
        Product  productA = this.productService.createProduct("A",new BigDecimal("5"));
        Checkout checkout = new CheckoutService(null);
        IntStream.range(0,20).forEach(t->checkout.scan(productA));
        IntStream.range(0,10).forEach(t->checkout.remove(productA));
        assertThat(checkout.getItemQty(productA)).isEqualTo(10);
    }

    @Test
    public void testTotalCheckout_addingItemsByPricingAndGetTotal_totalShouldBe41() throws Exception {
        // 10 / 3 = 3  * 12 + 1* 5 = 36 + 5 = 41
        BigDecimal total = new BigDecimal("41.00");
        Product  productA = this.productService.createProduct("A",new BigDecimal("5"));
        SpecialPricing pricingRule = new SpecialPricing(productA,3,new BigDecimal("12.00"));
        List<SpecialPricing> pricingRules = Collections.singletonList(pricingRule);
        Checkout checkout = new CheckoutService(pricingRules);
        IntStream.range(0,10).forEach(t->checkout.scan(productA));
        assertThat(checkout.total()).isEqualTo(total);
    }

}
