## Travello Coding Challenge

This project setup by using Gradle Groove java 11 

### How To Build 

to build this project please run :`./gradlew build`

### Test 
Junit used tp produce test and reporting.

To execute all the tests :`./gradlew test -i `

Test report will be created in `./test-reports` folder.

### How to run 

To run command line execute:`./gradlew run`

### How to use 

Run main class by executing `./gradlew run`

4 pre entered product exist in the A, B, C, D .
Product A and B having Special Pricing active : 
A $50 unit , 3 for $130
B $30 unit, 2 for $45

1. Enter each Product to add a scan into checkout bag .
1. Enter final to get total for the Bag .
1. Enter an exit to quit the program.

#### libraries used 

* lombok just to have cleaner classes.
* Slf4j for better logging.

#### Some notes 

All pricing and calculation using BigDecimal for better rounding and calculation.

Minimum validation used in models constructors to prevent errors.

Minimum user errors returns just for testing purposes .


#### author A.H.Safaie 





















